import {
  FETCH_PLACE_START,
  FETCH_PLACE_SUCCESS,
  FETCH_PLACE_FAIL,
} from '../../utils/constant';
import * as fetchHelper from '../../utils/fetchWrapper';
import {
  addAddress,
  deleteMarker,
  updateMarker,
  getAllAddress,
  fetchStart,
} from '../addMap.action';
import { placeData } from '../../reducers/__mock__/data';

describe('recipes action test suits', () => {
  it('places fetch success and fail test using promise', () => {
    const _url = 'mock url';
    const spy = jest
      .spyOn(fetchHelper, 'fetchWrapper')
      .mockImplementationOnce(_url => {
        return new Promise(resolve => resolve({ data: placeData }));
      })
      .mockImplementationOnce(_url => {
        return new Promise((resolve, reject) => reject());
      });
    getAllAddress().then(v => {
      expect(v).toEqual({
        type: FETCH_PLACE_SUCCESS,
        payload: { result: placeData, editKey: null },
      });
    });
    getAllAddress().then(v => {
      expect(v).toEqual({
        type: FETCH_PLACE_FAIL,
      });
    });
    getAllAddress();
    expect(spy).toHaveBeenCalled();
    spy.mockClear();
  });

  it('places start action test case', () => {
    const expectedAction = {
      type: FETCH_PLACE_START,
    };
    expect(fetchStart()).toEqual(expectedAction);
  });
});
