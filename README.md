### Bootstrap the app
- cd server
- npm start

- frontend [http://localhost:3000]
- api [http://localhost:5000]

test 
cd fronend
npm test


### Frontend Feature
- .env.devlopment and .env.production files for setting configuration related to app on dev and production phase
- prettier enables as pre commit hook
- firebase hosting with ci / cd pipeline in gitlab
- clear search field after submit success
- display toastr messages on success and faliure.

### API feature
- fetch all place
- add new place
- update new place
- delete new place

### Production App Details:
- Frontend:
- https://medwing-front.firebaseapp.com/findplaces

- API:
- https://us-central1-medwing-server.cloudfunctions.net/app/

### Frontend Tech stack
- React [create-react-app]
- Redux
- Sass
- thunk

## pending work
- code cleanup  / review
- wiring with concurrently with server
- mobile extra padding at bottom
- convrt api to put and delete